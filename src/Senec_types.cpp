#include "Senec_types.h"

#include <cinttypes>
#include <cstdio>
#include <cstdlib>
#include <cstring>

// SENEC sends the binary representation of the value.
// strtoul(l) will take care of the byte order issue.
// so no further conversion is needed

float convert_fl(const char *_raw) {
  static_assert(sizeof(float) == 4, "float must be 4 bytes.");
  union {
    uint32_t as_int;
    float as_float;
  } converter;
  converter.as_int = strtoul(_raw + 3, nullptr, 16);
  return converter.as_float;
}

// senec always sends the binary pattern. we can only interpret this as
// unsigned...
uint16_t convert_u1(const char *_raw) { return strtoul(_raw + 3, nullptr, 16); }
uint32_t convert_u3(const char *_raw) { return strtoul(_raw + 3, nullptr, 16); }
uint64_t convert_u6(const char *_raw) {
#if (__SIZEOF_LONG__ == 4)
  return strtoull(_raw + 3, nullptr, 16);
#else
  return strtoul(_raw + 3, nullptr, 16);
#endif
}
uint8_t convert_u8(const char *_raw) { return strtoul(_raw + 3, nullptr, 16); }

// ... and then silently convert to signed
int16_t convert_i1(const char *_raw) { return convert_u1(_raw); }
int32_t convert_i3(const char *_raw) { return convert_u3(_raw); }
int64_t convert_i6(const char *_raw) { return convert_u6(_raw); }
int8_t convert_i8(const char *_raw) { return convert_u8(_raw); }
