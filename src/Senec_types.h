#pragma once

#include <cstdint>
#include <string>

// float
float convert_fl(const char *_raw);

// signed integers
int16_t convert_i1(const char *_raw);
int32_t convert_i3(const char *_raw);
int64_t convert_i6(const char *_raw);
int8_t convert_i8(const char *_raw);

// unsigned integers
uint16_t convert_u1(const char *_raw);
uint32_t convert_u3(const char *_raw);
uint64_t convert_u6(const char *_raw);
uint8_t convert_u8(const char *_raw);
